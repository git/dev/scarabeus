# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

DESCRIPTION="Simple Serial Port Terminal"
HOMEPAGE="http://dev.frozeneskimo.com/software_projects/ssterm"
SRC_URI="http://www.frozeneskimo.com/software/ssterm/${P}.tar.gz"

LICENSE="GPL"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=" sys-libs/ncurses "
RDEPEND="${DEPEND}"

src_compile() {
	emake || die "compile failed"
}

src_install() {
	dobin ${WORKDIR}/${P}/ssterm || die "install failed"
}
