# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

DESCRIPTION="vPICdisasm Microchip PIC Disassembler"
HOMEPAGE="http://dev.frozeneskimo.com/software_projects/vpicdisasm"
SRC_URI="http://www.frozeneskimo.com/software/vpicdisasm/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=" "
RDEPEND="${DEPEND}"

src_compile() {
	emake || die "compile failed"
}

src_install() {
	dobin ${WORKDIR}/${P}/vpicdisasm || die "install failed"
}
