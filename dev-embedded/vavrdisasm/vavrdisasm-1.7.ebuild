# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

DESCRIPTION="vAVRdisasm Atmel AVR Disassembler"
HOMEPAGE="http://dev.frozeneskimo.com/software_projects/vavrdisasm"
SRC_URI="http://www.frozeneskimo.com/software/vavrdisasm/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=" "
RDEPEND="${DEPEND}"

src_compile() {
	emake || die "compile failed"
}

src_install() {
	dobin ${WORKDIR}/${P}/vavrdisasm || die "install failed"
}
