# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

EGIT_REPO_URI="https://github.com/divVerent/s2tc.git"
inherit git-2 autotools

DESCRIPTION="s2tc (non-patented s3tc) texture compression library"
HOMEPAGE="https://github.com/divVerent/s2tc/wiki/"

LICENSE="as-is"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="virtual/opengl"
RDEPEND="${DEPEND}
	!x11-libs/libtxc_dxtn
"

src_prepare() {
	eautoreconf
}

src_configure() {
	econf \
		--disable-static \
		--disable-runtime-linking \
		--enable-lib \
		--enable-tools
}

src_install() {
	default
	find "${ED}" -name '*.la' -exec rm -f {} +
}
