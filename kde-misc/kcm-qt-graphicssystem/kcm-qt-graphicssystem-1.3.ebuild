# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit kde4-base

DESCRIPTION="Setter for Qt rendering engine."
HOMEPAGE="http://kde-apps.org/content/show.php/KCM+Qt+Graphics+System?content=129817"
SRC_URI="http://kde-apps.org/CONTENT/content-files/129817-${P}.tar.xz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux"
IUSE=""
